package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ClassifierInfo {


    private List<String> customerTypes;
	private List<String> phoneTypes;

}


