package customer.DAO;

import model.Customer;
import util.JpaUtil;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;

public class CustomerDAO {

    private static List<Customer> customers = new ArrayList<>();

    public void addCustomer(Customer customer) {

        EntityManager em = null;
        try {
            em = JpaUtil.getFactory().createEntityManager();
            em.getTransaction().begin();
            if(!em.contains(customer)) {
                em.persist(customer);
            }
            em.persist(customer);
            em.getTransaction().commit();
        } finally {
            JpaUtil.closeQuietly(em);
        }
    }

    public List<Customer> getAll() {

        EntityManager em = null;
        try {
            em = JpaUtil.getFactory().createEntityManager();

            TypedQuery<Customer> query = em.createQuery("select c from Customer c", Customer.class);

            return query.getResultList();
        } finally {
            JpaUtil.closeQuietly(em);
        }
    }

    public void deleteAll(){


        EntityManager em = null;
        try {
            em = JpaUtil.getFactory().createEntityManager();
            em.getTransaction().begin();
            Query query = em.createQuery("DELETE FROM Customer");
            query.executeUpdate();

            em.getTransaction().commit();
        } finally {
            JpaUtil.closeQuietly(em);
        }
//        for (Customer c : getAll()) {
//            deleteCustomerById(c.getId());
//        }
    }

    public  Customer getCustomerById(Long id) {

        EntityManager em = null;
        try {
            em = JpaUtil.getFactory().createEntityManager();
            return em.find(Customer.class, id);
        } finally {
            JpaUtil.closeQuietly(em);
        }
    }

    public void deleteCustomerById(Long id) {

        Customer customer = getCustomerById(id);
        EntityManager em = null;
        try {
            em = JpaUtil.getFactory().createEntityManager();
            em.getTransaction().begin();
            if (em.contains(customer)) {
                em.remove(customer);
                em.getTransaction().commit();
            }
        } finally {
            JpaUtil.closeQuietly(em);
        }
    }

    public void printAllCustomers() {
        System.out.println(customers.toString());
    }
}
