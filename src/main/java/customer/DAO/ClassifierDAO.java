package customer.DAO;

import java.util.ArrayList;
import java.util.List;

public class ClassifierDAO {

    public List<String> getCustomerTypes() {
        List<String> customerTypes = new ArrayList<>();
        customerTypes.add("customer_type.private");
        customerTypes.add("customer_type.corporate");

        return customerTypes;
    }

    public List<String> getPhoneTypes() {
        List<String> phoneTypes = new ArrayList<>();
        phoneTypes.add("phone_type.fixed");
        phoneTypes.add("phone_type.mobile");

        return phoneTypes;
    }
}
