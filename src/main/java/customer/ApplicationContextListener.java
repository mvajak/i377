package customer;

import util.DataSourceProvider;
import util.DbUtil;
import util.FileUtil;
import util.PropertyLoader;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.sql.Connection;
import java.sql.SQLException;

@WebListener
public class ApplicationContextListener implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent sce) {

        String url = PropertyLoader.getProperty("jdbc.url");
        String schema = FileUtil.readFileFromClasspath("schema.sql");
        DataSourceProvider.setDbUrl(url);

        try (Connection conn = DataSourceProvider.getDataSource().getConnection()){
            DbUtil.insertFromFile(conn, schema);
        } catch (SQLException e){
            throw new RuntimeException();
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }
}