package servlets;


import com.fasterxml.jackson.databind.ObjectMapper;
import customer.DAO.ClassifierDAO;
import model.ClassifierInfo;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/api/classifiers")
public class ClassifierServiceServlet extends HttpServlet{

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        ClassifierDAO classifierDAO = new ClassifierDAO();

        response.setContentType("application/json");

        ClassifierInfo info = new ClassifierInfo(classifierDAO.getCustomerTypes(), classifierDAO.getPhoneTypes());

        new ObjectMapper().writeValue(response.getOutputStream(), info);

    }
}
