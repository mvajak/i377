package servlets;

import model.Customer;
import customer.DAO.CustomerDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/customers/form")
public class CustomersFormServlet extends HttpServlet{
    private static final long serialVersionUID = 1L;

    private CustomerDAO dao = new CustomerDAO();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        resp.setContentType("text/html");
        String name = req.getParameter("name");

        Customer customer = new Customer();
        customer.setFirstName(name);

        dao.addCustomer(customer);
        dao.printAllCustomers();
        resp.setStatus(HttpServletResponse.SC_OK);
    }
}
