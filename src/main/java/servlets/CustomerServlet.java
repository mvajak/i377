package servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import model.Customer;
import customer.DAO.CustomerDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet("/api/customers")
public class CustomerServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private CustomerDAO dao = new CustomerDAO();

    @Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String id = req.getParameter("id");
        ObjectMapper map = new ObjectMapper();
        resp.setContentType("application/json");

        if (id != null) {
            long longId = Long.parseLong(id);
            resp.getWriter().print(map.writeValueAsString(dao.getCustomerById(longId)));

        } else {
            resp.getWriter().print(new ObjectMapper().writeValueAsString(dao.getAll()));
        }
    }

    @Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Customer customer = new ObjectMapper().readValue(req.getInputStream(), Customer.class);

        resp.setContentType("application/json");

        dao.addCustomer(customer);

        resp.setStatus(HttpServletResponse.SC_OK);
    }

    @Override
	protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");

        String reqPar = req.getParameter("id");

        if (reqPar != null) {
            Long id = Long.parseLong(req.getParameter("id"));
            dao.deleteCustomerById(id);
        } else {
            dao.deleteAll();
        }
        resp.setStatus(HttpServletResponse.SC_OK);
    }

}
